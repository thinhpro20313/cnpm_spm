#Spm
# 21DTHD4-2180608631-VDT
Vũ Đức Thịnh
21DTHD4
2180608631 <br/>
| Title | Manager search for a product using ID |
| :---: |---| 
| Value statement | As a manager , I wanna find a necessary product item |
| Acceptance criteria | <ins> Acceptance criterion 1:</ins><br/> When I want to find a product, I will enter the product's ID then system check it.<br/> If product's ID is true, display the product's information when the requirements are met Or display warning error.|<br/>
| Definition of Done| <br/>Unit Tests Passed, <br/> Accetance Criteria Met, <br/> Code Reviewed, <br/> Functional Tests Passed, <br/> Non-Functional Requirements Met, <br/> Product Owner Accepts User Story|
|Owner| Vu Duc Thinh|


| Title | Manager classify product using ID |
| :---: |---| 
| Value statement | As a manager , I wanna find a necessary product item |
| Acceptance criteria | <ins> Acceptance criterion 1:</ins><br/> When I want to classify a product, I will enter the classification conditions <br/> then system compare the input with the data from database <br/> and display datas that similar to the input. |<br/>
| Definition of Done| <br/>Unit Tests Passed, <br/> Accetance Criteria Met, <br/> Code Reviewed, <br/> Functional Tests Passed, <br/> Non-Functional Requirements Met, <br/> Product Owner Accepts User Story|
|Owner| Vu Duc Thinh|

| Title | Manager search for a invoice  |
| :---: |---| 
| Value statement | As a manager , I wanna find a necessary invoice item |
| Acceptance criteria | <ins> Acceptance criterion 1:</ins><br/> When I want to find a invoice, I will enter the invoice's ID then system check it.<br/> If invoice's ID is true, display the invoice's information when the requirements are met Or display warning error.|<br/>
| Definition of Done| <br/>Unit Tests Passed, <br/> Accetance Criteria Met, <br/> Code Reviewed, <br/> Functional Tests Passed, <br/> Non-Functional Requirements Met, <br/> Product Owner Accepts User Story|
|Owner| Vu Duc Thinh|


| Title | Manager classify invoice |
| :---: |---| 
| Value statement | As a manager , I wanna find a necessary invoice item |
| Acceptance criteria | <ins> Acceptance criterion 1:</ins><br/> When I want to classify a invoice, I will enter the classification conditions <br/> then system compare the input with the data from database <br/> and display datas that similar to the input. |<br/>
| Definition of Done| <br/>Unit Tests Passed, <br/> Accetance Criteria Met, <br/> Code Reviewed, <br/> Functional Tests Passed, <br/> Non-Functional Requirements Met, <br/> Product Owner Accepts User Story|
|Owner| Vu Duc Thinh|

